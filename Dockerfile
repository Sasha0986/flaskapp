FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN apt-get update && apt-get install
RUN apt-get install iputils-ping -y
RUN python -m pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt
RUN echo "192.168.0.52    db.shishko.info" >> /etc/hosts
COPY ./finaltask/webserver/appliance .

CMD [ "python", "./app.py"]
